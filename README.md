# CSVTOXML

CSV2XML allows you to convert a CSV file format to an XML file format without effort. The only thing you need is to have a CSV file with the first row containing the names of the fields. You can then choose between a comma, semicolon, tab or vertical bar delimiter before specifying the row's name and launch the convertion.

When you launch the convertion, a progress-bar let you know the progress of the job.

This little piece of software is lightweight, but powerful and useful. You always need that kind of stuff close at hand.

This is an old project that is not maintained anymore.

Feel free to fork it and do whatever you want with it.

It was developped with the NetBeans IDE

Under [MIT License](LICENSE.md)
