# CSVTOXML CHANGE LOG by Ibbtek

## Version 1.3

- Include & read Help, About & Licenses files from inside the *.jar
- Change ugly orange progressbar color to blue
- Minor bugs fixed

## Version 1.2

- Use DOM Transformer to create the XML instead of simple outputStream
- Add Linux and Mac capacity to open default browser for HELP and LICENSES files
- Add cancel button to cancel the convertion
